﻿using System.Data;

namespace DAO.Interfaces
{
    public interface ICrudBase<T>
    {
        int Insert(T t);
        int Update(T t);
        int Delete(T t);
        DataTable Select();
    }
}