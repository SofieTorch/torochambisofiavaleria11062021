﻿using System.Data;
using System.Data.SqlClient;

namespace DAO.Interfaces
{
    public interface ICliente
    {
        SqlDataReader SelectDatosCliente(int clienteId);
        SqlDataReader SelectComprasDatosCliente(int clienteId);
        DataTable SelectProductosAdquiridos(int clienteId);
    }
}