﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using DAO.Implementation;
using DAO.Models;

namespace ToroChambiSofiaValeria11062021UserInterface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnObtenerDatosConsulta_OnClick(object sender, RoutedEventArgs e)
        {
            int clienteId = Int32.Parse(TxbClienteID.Text);
            MostrarDatosCliente(clienteId);
            MostrarDatosComprasCliente(clienteId);
            MostrarDatosProductos(clienteId);
        }

        private void MostrarDatosCliente(int clienteId)
        {
            ClienteImpl clienteImpl = new ClienteImpl();
            SqlDataReader dr = clienteImpl.SelectDatosCliente(clienteId);
            
            try
            {
                dr.Read();
                Cliente cliente = new Cliente(clienteId, dr[0].ToString(), dr[1].ToString());
                LblNombre.Content = cliente.Nombre;
                LblDireccion.Content = cliente.Direccion;
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                dr.Close();
            }
        }
        
        private void MostrarDatosComprasCliente(int clienteId)
        {
            ClienteImpl clienteImpl = new ClienteImpl();
            SqlDataReader dr = clienteImpl.SelectComprasDatosCliente(clienteId);
            
            try
            {
                dr.Read();
                LblCompras.Content = dr[0].ToString();
                LblTotalCompras.Content = dr[1].ToString();
            }
            catch (Exception exception)
            {
                throw exception;
            }
            finally
            {
                dr.Close();
            }
        }

        private void MostrarDatosProductos(int clienteId)
        {
            ClienteImpl clienteImpl = new ClienteImpl();
            try
            {
                DataTable dt = clienteImpl.SelectProductosAdquiridos(clienteId);
                DataGrid.ItemsSource = dt.DefaultView;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        
        
    }
}