﻿namespace DAO.Models
{
    public class Cliente
    {
        public int ClienteId { get; set; }
        public string Nombre { get; set; }
        public string Direccion { get; set; }
        public byte Activo { get; set; }
        public int CiudadId { get; set; }

        /// <summary>
        /// Constructor for complete SELECT
        /// </summary>
        /// <param name="clienteId"></param>
        /// <param name="nombre"></param>
        /// <param name="direccion"></param>
        /// <param name="activo"></param>
        /// <param name="ciudadId"></param>
        public Cliente(int clienteId, string nombre, string direccion, byte activo, int ciudadId)
        {
            ClienteId = clienteId;
            Nombre = nombre;
            Direccion = direccion;
            Activo = activo;
            CiudadId = ciudadId;
        }

        public Cliente(int clienteId, string nombre, string direccion)
        {
            ClienteId = clienteId;
            Nombre = nombre;
            Direccion = direccion;
        }
    }
}