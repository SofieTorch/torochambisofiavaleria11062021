﻿using System;

namespace DAO.Models
{
    public class BaseAudit
    {
        #region Properties
        public DateTime CreationDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public short RegisteredBy { get; set; }
        public byte RecordStatus { get; set; }
        
        #endregion

        #region Constructors

        public BaseAudit() { }
        
        /// <summary>
        /// Constructor for queries of type SELECT
        /// </summary>
        /// <param name="creationDate"></param>
        /// <param name="lastUpdateDate"></param>
        /// <param name="registeredBy"></param>
        /// <param name="recordStatus"></param>
        public BaseAudit(
            DateTime creationDate, DateTime lastUpdateDate,
            short registeredBy, byte recordStatus)
        {
            CreationDate = creationDate;
            LastUpdateDate = lastUpdateDate;
            RegisteredBy = registeredBy;
            RecordStatus = recordStatus;
        }

        /// <summary>
        /// Constructor for queries of type INSERT
        /// </summary>
        /// <param name="registeredBy"></param>
        public BaseAudit(short registeredBy)
        {
            RegisteredBy = registeredBy;
        }

        #endregion

    }
}