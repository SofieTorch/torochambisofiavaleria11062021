﻿using System;
using System.Data;
using System.Data.SqlClient;
using DAO.Interfaces;

namespace DAO.Implementation
{
    public class ClienteImpl : ICliente
    {
        public SqlDataReader SelectDatosCliente(int clienteId)
        {
            try
            {
                string query = @"
                    IF EXISTS (
                        SELECT Nombre, Direccion
                        FROM Cliente
                        WHERE ClienteID = @ClienteId) 
                    BEGIN
                        SELECT Nombre, Direccion
                        FROM Cliente
                        WHERE ClienteID = @ClienteId
                    END
                        ELSE
                    BEGIN
                        SELECT 'NO EXISTE UN CLIENTE CON EL ID INGRESADO',
                        'NO EXISTE UN CLIENTE CON EL ID INGRESADO'
                    END";
                
                DatabaseConnection dbconn = new DatabaseConnection();
                SqlCommand command = dbconn.CreateCommand(query);
                command.Parameters.AddWithValue("@ClienteId", clienteId);
                return DatabaseConnection.ExecuteDataReaderCommand(command);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public SqlDataReader SelectComprasDatosCliente(int clienteId)
        {
            try
            {
                string query = @"
                    SELECT COUNT(*), ISNULL(SUM(total), 0)
                    FROM Compra
                    INNER JOIN Cliente ON Cliente.ClienteID = Compra.ClienteID
                    WHERE Cliente.ClienteID = @ClienteId";
                
                DatabaseConnection dbconn = new DatabaseConnection();
                SqlCommand command = dbconn.CreateCommand(query);
                command.Parameters.AddWithValue("@ClienteId", clienteId);
                return DatabaseConnection.ExecuteDataReaderCommand(command);
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public DataTable SelectProductosAdquiridos(int clienteId)
        {
            try
            {
                string query = @"
                    SELECT P.Nombre, CD.cantidad
                    FROM Productos P
                    INNER JOIN CompraDetalle CD ON CD.ProductoID = P.ProductoID
                    INNER JOIN Compra Co ON CD.CompraID = Co.CompraID
                    INNER JOIN Cliente Cl ON Co.ClienteID = Cl.ClienteID
                    WHERE Cl.ClienteID = @ClienteId";
                
                DatabaseConnection dbconn = new DatabaseConnection();
                SqlCommand command = dbconn.CreateCommand(query);
                command.Parameters.AddWithValue("@ClienteId", clienteId);
                return DatabaseConnection.ExecuteSelectCommand(command);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}