﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace DAO.Implementation
{
    public class DatabaseConnection
    {
        private static string databaseUri = "data source = localhost; initial catalog = ComprasHelpDesk; user id = sa; password = Hemmo1996";

        public SqlCommand CreateCommand()
        {
            SqlConnection connection = new SqlConnection(databaseUri);
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
            return command;
        }
        
        public SqlCommand CreateCommand(string query)
        {
            SqlConnection connection = new SqlConnection(databaseUri);
            SqlCommand command = new SqlCommand(query);
            command.Connection = connection;
            return command;
        }

        public static int ExecuteCommand(SqlCommand command)
        {
            try
            {
                command.Connection.Open();
                return command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                command.Connection.Close();
            }
        }

        public static DataTable ExecuteSelectCommand(SqlCommand command)
        {
            DataTable res = new DataTable();
            try
            {
                command.Connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                adapter.Fill(res);
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                command.Connection.Close();
            }

            return res;
        }

        public static SqlDataReader ExecuteDataReaderCommand(SqlCommand command)
        {
            SqlDataReader dr = null;
            try
            {
                command.Connection.Open();
                dr = command.ExecuteReader();
            }
            catch (Exception e)
            {
                throw e;
            }

            return dr;
        }

    }
}